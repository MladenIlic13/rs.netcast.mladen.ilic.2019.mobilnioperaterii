package mobilniOperaterBaseII;

import java.util.ArrayList;
import java.util.Scanner;

public class TarifniDodatak {

	private MobilniOperater mobilniOperater;
	
	private String tarifniDodatak;

	private int tarifniDodatakCena;
	
	public TarifniDodatak() {
	}
	
	public TarifniDodatak(MobilniOperater mobilniOperater, String tarifniDodatak,
			int tarifniDodatakCena) {
		this.mobilniOperater = mobilniOperater;
		this.tarifniDodatak = tarifniDodatak;
		this.tarifniDodatakCena = tarifniDodatakCena;
	}

	public String getTarifniDodatak() {
		return tarifniDodatak;
	}

	public void setTarifniDodatak(String tarifniDodatak) {
		this.tarifniDodatak = tarifniDodatak;
	}

	public MobilniOperater getMobilniOperater() {
		return mobilniOperater;
	}

	public void setMobilniOperater(MobilniOperater mobilniOperater) {
		this.mobilniOperater = mobilniOperater;
	}

	public int getTarifniDodatakCena() {
		return tarifniDodatakCena;
	}

	public void setTarifniDodatakCena(int tarifniDodatakCena) {
		this.tarifniDodatakCena = tarifniDodatakCena;
	}

	@Override
	public String toString() {
		return "TarifniDodatak [mobilniOperater=" + mobilniOperater + ", tarifniDodatak=" + tarifniDodatak
				+ ", tarifniDodatakCena=" + tarifniDodatakCena + "]";
	}
	
	// @@@@@@@@@@
	// logic
	
	public static void izlistajTarifneDodatke(MobilniOperater mobOp) {
		System.out.println("dostupni dodaci za " + mobOp.getName() + " su:");
		for (int i=0; i<mobOp.getDodaci().size(); i++) {
			TarifniDodatak tarifniDodatak = (TarifniDodatak)mobOp.getDodaci().get(i);
			System.out.println(tarifniDodatak.getTarifniDodatak());
		}
		System.out.println();
	}
	
	// izbor tarifnog dodatka
	public static TarifniDodatak matchujTarifniDodatak(MobilniOperater mobOp, String matchIme) {
		TarifniDodatak tarifniDodatak = null;
		for (int i=0; i<mobOp.getDodaci().size(); i++) {
			tarifniDodatak = (TarifniDodatak)mobOp.getDodaci().get(i);
			if (tarifniDodatak.getTarifniDodatak() == matchIme) {
				break;
			}
		}
		return tarifniDodatak;
	}
	
	public static TarifniDodatak dodajTarifniDodatak(MobilniOperater mobOp) {
		
		Scanner scannerString = new Scanner(System.in);
		Scanner scannerInt = new Scanner(System.in);
		
		System.out.println("unesi naziv novog dodatka");
		String name = scannerString.nextLine();
		System.out.println("unesi cenu novog dodatka");
		int cena = scannerInt.nextInt();	

		TarifniDodatak tarifniDodatak = new TarifniDodatak(mobOp, name, cena);
		mobOp.getDodaci().add(tarifniDodatak);
		
		System.out.println(tarifniDodatak.toString());
		System.out.println(mobOp.getDodaci().toString());
		System.out.println();
		
		return tarifniDodatak;

	}

}
