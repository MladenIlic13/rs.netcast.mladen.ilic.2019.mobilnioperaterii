package mobilniOperaterBaseII;

import java.util.ArrayList;
import java.util.Scanner;

public abstract class Korisnik{

	private String naziv;
	
	private int minuta;
	
	private int poruka;
	
	private int mbSurf;
	
	private int stanje;
	
	private PostpaidTarifa postpaidTarifa;

	ArrayList<TarifniDodatak> tarifniDodaci;

	public Korisnik() {
	}

	public Korisnik(String naziv, int minuta, int poruka, int mbSurf, int stanje, PostpaidTarifa postpaidTarifa,
			ArrayList<TarifniDodatak> tarifniDodaci) {
		this.naziv = naziv;
		this.minuta = minuta;
		this.poruka = poruka;
		this.mbSurf = mbSurf;
		this.stanje = stanje;
		this.postpaidTarifa = postpaidTarifa;
		this.tarifniDodaci = tarifniDodaci;
	}

	public int getMinuta() {
		return minuta;
	}

	public void setMinuta(int minuta) {
		this.minuta = minuta;
	}

	public int getPoruka() {
		return poruka;
	}

	public void setPoruka(int poruka) {
		this.poruka = poruka;
	}

	public int getMbSurf() {
		return mbSurf;
	}

	public void setMbSurf(int mbSurf) {
		this.mbSurf = mbSurf;
	}

	public ArrayList<TarifniDodatak> getTarifniDodaci() {
		return tarifniDodaci;
	}

	public void setTarifniDodaci(ArrayList<TarifniDodatak> tarifniDodaci) {
		this.tarifniDodaci = tarifniDodaci;
	}

	public int getStanje() {
		return stanje;
	}

	public void setStanje(int stanje) {
		this.stanje = stanje;
	}

	public PostpaidTarifa getPostpaidTarifa() {
		return postpaidTarifa;
	}

	public void setPostpaidTarifa(PostpaidTarifa postpaidTarifa) {
		this.postpaidTarifa = postpaidTarifa;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	@Override
	public String toString() {
		return "Korisnik [naziv=" + naziv + ", minuta=" + minuta + ", poruka=" + poruka + ", mbSurf=" + mbSurf
				+ ", stanje=" + stanje + ", postpaidTarifa=" + postpaidTarifa + ", tarifniDodaci=" + tarifniDodaci
				+ "]";
	}
	
	// listaj za konkretnu tarifu
		public static void izlistajPostpaidKorisnike() {
			System.out.println("postpaid korisnici su:");
			for (int i=0; i<KorisnikPostpaidPrivate.postpaidKorisniciPrivate.size(); i++) {
				KorisnikPostpaidPrivate korisnik = (KorisnikPostpaidPrivate)KorisnikPostpaidPrivate.postpaidKorisniciPrivate.get(i);
				System.out.println(korisnik.getNaziv());
			}
			for (int i=0; i<KorisnikPostpaidBusiness.postpaidKorisniciBusiness.size(); i++) {
				KorisnikPostpaidBusiness korisnik = (KorisnikPostpaidBusiness)KorisnikPostpaidBusiness.postpaidKorisniciBusiness.get(i);
				System.out.println(korisnik.getNaziv());
			}
			System.out.println();
		}
		
		// izbor korisnika
		public static Korisnik matchujPostpaidKorisnika(String matchIme) {
			Korisnik korisnik1 = null;
			for (int i=0; i<KorisnikPostpaidPrivate.postpaidKorisniciPrivate.size(); i++) {
				korisnik1 = (Korisnik)KorisnikPostpaidPrivate.postpaidKorisniciPrivate.get(i);
				if (korisnik1.getNaziv().equals(matchIme)) {
					break;
				}
			}
			Korisnik korisnik2 = null;
			for (int i=0; i<KorisnikPostpaidBusiness.postpaidKorisniciBusiness.size(); i++) {
				korisnik2 = (Korisnik)KorisnikPostpaidBusiness.postpaidKorisniciBusiness.get(i);
				if (korisnik2.getNaziv().equals(matchIme)) {
					break;
				}
			}
			if (korisnik1 != null) {
				return korisnik1;
			} else if (korisnik2 != null) {
				return korisnik2;
			} else {
				System.out.println("error");
				return korisnik1;
			}
			
		}
		
		public static Korisnik ucitajPostpaidKorisnika() {
			izlistajPostpaidKorisnike();
			System.out.println("izaberi postpaid korisnika");
			Scanner scannerString = new Scanner(System.in);
			String name = scannerString.nextLine();
			Korisnik korisnik = matchujPostpaidKorisnika(name);
			Main.korPost = korisnik;
			System.out.println();
			return korisnik;
		}

}
