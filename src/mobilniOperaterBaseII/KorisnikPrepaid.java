package mobilniOperaterBaseII;

import java.util.ArrayList;
import java.util.Scanner;

public class KorisnikPrepaid implements Tarifa{

	public static ArrayList<KorisnikPrepaid> prepaidKorisnici = new ArrayList<>();
	
	private String naziv;
	
	private PrepaidTarifa prepaidTarifa;
	
	private int prepaidKredit;

	public KorisnikPrepaid() {
	}

	public KorisnikPrepaid(String naziv, PrepaidTarifa prepaidTarifa, int prepaidKredit) {
		this.naziv = naziv;
		this.prepaidTarifa = prepaidTarifa;
		this.prepaidKredit = prepaidKredit;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public PrepaidTarifa getPrepaidTarifa() {
		return prepaidTarifa;
	}

	public void setPrepaidTarifa(PrepaidTarifa prepaidTarifa) {
		this.prepaidTarifa = prepaidTarifa;
	}

	public int getPrepaidKredit() {
		return prepaidKredit;
	}

	public void setPrepaidKredit(int prepaidKredit) {
		this.prepaidKredit = prepaidKredit;
	}

	@Override
	public String toString() {
		return "KorisnikPrepaid [naziv=" + naziv + ", prepaidTarifa=" + prepaidTarifa + ", prepaidKredit="
				+ prepaidKredit + "]";
	}
	
	// @@@@@@@@@@@@@@
	// logic
		
	public static KorisnikPrepaid dodajPrepaidKorisnika(PrepaidTarifa tarifa) {
		
		Scanner scannerString = new Scanner(System.in);
		
		// naziv proveri duplikat
		String korisnikIme = null;
		Loop:
		while(true) {	
			System.out.println("unesi naziv prepaid korisnika");
			korisnikIme = scannerString.nextLine();
			if (prepaidKorisnici.size()==0) {
				break Loop;
			}
			int x = 0;
			for (int i=0; i<KorisnikPostpaidPrivate.postpaidKorisniciPrivate.size(); i++) {
				KorisnikPostpaidPrivate korisnik = (KorisnikPostpaidPrivate)KorisnikPostpaidPrivate.postpaidKorisniciPrivate.get(i);
				if (korisnik.getNaziv().equals(korisnikIme)) {
					x++;
				} 
			} for (int i=0; i<KorisnikPostpaidBusiness.postpaidKorisniciBusiness.size(); i++) {
				KorisnikPostpaidBusiness korisnik = (KorisnikPostpaidBusiness)KorisnikPostpaidBusiness.postpaidKorisniciBusiness.get(i);
				if (korisnik.getNaziv().equals(korisnikIme)) {
					x++;
				} 
			} for (int i=0; i<KorisnikPrepaid.prepaidKorisnici.size(); i++) {
				KorisnikPrepaid korisnik = (KorisnikPrepaid)KorisnikPrepaid.prepaidKorisnici.get(i);
				if (korisnik.getNaziv().equals(korisnikIme)) {
					x++;
				} 
			}
			if (x == 0) {
				break;
			} else {
				System.out.println("duplikat, unesite ponovo");
				continue;
			}
		}
		
//		scannerString.close();
		
		int kredit = tarifa.getPrepaidKredit();

		KorisnikPrepaid korisnikPrepaid = new KorisnikPrepaid(korisnikIme, tarifa, kredit);
		prepaidKorisnici.add(korisnikPrepaid);
		Main.korPre = korisnikPrepaid;
		
		System.out.println(korisnikPrepaid.toString());
		System.out.println(prepaidKorisnici.toString());
		System.out.println();

		return korisnikPrepaid;
		
	}
	
	// listaj za konkretnu tarifu
	public static void izlistajPrepaidKorisnike(PrepaidTarifa tarifa) {
		System.out.println("korisnici sa tarifom " + tarifa.getTarifa() + " su:");
		for (int i=0; i<prepaidKorisnici.size(); i++) {
			KorisnikPrepaid korisnik = (KorisnikPrepaid)prepaidKorisnici.get(i);
			if (korisnik.getPrepaidTarifa().getTarifa().equals(tarifa.getTarifa())) {
				System.out.println(korisnik.getNaziv());
			}
		}
		System.out.println();
	}
	
	// izbor korisnika
	public static KorisnikPrepaid matchujPrepaidKorisnika(String matchIme) {
		KorisnikPrepaid korisnik = null;
		for (int i=0; i<prepaidKorisnici.size(); i++) {
			korisnik = (KorisnikPrepaid)prepaidKorisnici.get(i);
			if (korisnik.getNaziv().equals(matchIme)) {
				break;
			}
		}
		return korisnik;
	}
	
	public static KorisnikPrepaid ucitajPrepaidKorisnika(PrepaidTarifa tarifa) {
		izlistajPrepaidKorisnike(tarifa);
		System.out.println("izaberi prepaid korisnika");
		Scanner scannerString = new Scanner(System.in);
		String name = scannerString.nextLine();
		KorisnikPrepaid korisnik = matchujPrepaidKorisnika(name);
		Main.korPre = korisnik;
		System.out.println();
		return korisnik;
	}

	@Override
	public boolean razgovor() {
		System.out.println("koliko zelite da razgoravate minuta");
		Scanner sc = new Scanner(System.in);
		int minuta = sc.nextInt();
		int cenaRazgovora = this.getPrepaidTarifa().getMobilniOperater().getMinPrice() * minuta;
		if (this.getPrepaidKredit() < cenaRazgovora) {
			System.out.println("nemate dovoljno kredita");
			return false;
		} else {
			this.setPrepaidKredit(this.getPrepaidKredit() - cenaRazgovora);
			String string = "prepaid korisnik " + this.getNaziv() + ", tarifa "
					+ this.getPrepaidTarifa().getTarifa() + ", operater "
					+ this.getPrepaidTarifa().getMobilniOperater().getName()
					+ ", utroseno minuta " + minuta;
			MobilniOperater.log.add(string);
			System.out.println("uspesno obavljen razgovor");
			System.out.println();
			return true;
		}
	}

	@Override
	public boolean slanjePoruka() {
		int cenaPoruke = this.getPrepaidTarifa().getMobilniOperater().getMessagePrice();
		if (this.getPrepaidKredit() < cenaPoruke) {
			System.out.println("nemate dovoljno kredita");
			return false;
		} else {
			this.setPrepaidKredit(this.getPrepaidKredit() - cenaPoruke);
			System.out.println("unesite poruku");
			Scanner sc = new Scanner(System.in);
			String poruka = sc.nextLine();
			String string = "prepaid korisnik " + this.getNaziv() + ", tarifa "
					+ this.getPrepaidTarifa().getTarifa() + ", operater "
					+ this.getPrepaidTarifa().getMobilniOperater().getName()
					+ ", poslata poruka <<" + poruka + ">>";
			MobilniOperater.log.add(string);
			System.out.println("uspesno poslata poruka");
			System.out.println();
			return true;
		}
	}

	@Override
	public boolean surf() {
		System.out.println("koliko mb zelite da potrosite");
		Scanner sc = new Scanner(System.in);
		int brojMb = sc.nextInt();
		int cenaSurfa = this.getPrepaidTarifa().getMobilniOperater().getMbPrice() * brojMb;
		if (this.getPrepaidKredit() < cenaSurfa) {
			System.out.println("nemate dovoljno kredita");
			return false;
		} else {
			this.setPrepaidKredit(this.getPrepaidKredit() - cenaSurfa);
			System.out.println("unesite url");
			Scanner sc2 = new Scanner(System.in);
			String url = sc2.nextLine();
			String string = "prepaid korisnik " + this.getNaziv() + ", tarifa "
					+ this.getPrepaidTarifa().getTarifa() + ", operater "
					+ this.getPrepaidTarifa().getMobilniOperater().getName()
					+ ", posecen url <<" + url + ">>" + ", potroseno mb " + brojMb;
			MobilniOperater.log.add(string);
			System.out.println("uspesno posecen sajt");
			System.out.println();
			return true;
		}
	}
	
}
