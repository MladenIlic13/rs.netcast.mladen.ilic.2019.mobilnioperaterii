package mobilniOperaterBaseII;

import java.util.ArrayList;
import java.util.Scanner;

public class PostpaidTarifa {

	public static ArrayList<PostpaidTarifa> postpaidTarife = new ArrayList<>();
	
	String tarifa;
	
	MobilniOperater mobilniOperater;
	
	boolean business;
	
	int minuta;
	
	int poruka;
	
	int mb;

	public PostpaidTarifa() {
	}

	public PostpaidTarifa(String tarifa, MobilniOperater mobilniOperater, boolean business, int minuta, int poruka,
			int mb) {
		super();
		this.tarifa = tarifa;
		this.mobilniOperater = mobilniOperater;
		this.business = business;
		this.minuta = minuta;
		this.poruka = poruka;
		this.mb = mb;
	}

	public String getTarifa() {
		return tarifa;
	}

	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}

	public MobilniOperater getMobilniOperater() {
		return mobilniOperater;
	}

	public void setMobilniOperater(MobilniOperater mobilniOperater) {
		this.mobilniOperater = mobilniOperater;
	}

	public boolean isBusiness() {
		return business;
	}

	public void setBusiness(boolean business) {
		this.business = business;
	}

	public int getMinuta() {
		return minuta;
	}

	public void setMinuta(int minuta) {
		this.minuta = minuta;
	}

	public int getPoruka() {
		return poruka;
	}

	public void setPoruka(int poruka) {
		this.poruka = poruka;
	}

	public int getMb() {
		return mb;
	}

	public void setMb(int mb) {
		this.mb = mb;
	}

	@Override
	public String toString() {
		return "PostpaidTarifa [tarifa=" + tarifa + ", mobilniOperater=" + mobilniOperater + ", business=" + business
				+ ", minuta=" + minuta + ", poruka=" + poruka + ", mb=" + mb + "]";
	}
	
	// @@@@@@@@@@@@
	// logic
	
	// izbor tarife
	public static PostpaidTarifa matchujPostpaidTarifu(String matchIme) {
		PostpaidTarifa tarifa = null;
		for (int i=0; i<postpaidTarife.size(); i++) {
			tarifa = (PostpaidTarifa)postpaidTarife.get(i);
			if (tarifa.getTarifa().equals(matchIme)) {
				break;
			}
		}
		return tarifa;
	}
	
	// listaj za konkretnog operatera
	public static void izlistajPostpaidTarife(MobilniOperater mobOp) {
		System.out.println("dostupne postpaid tarife za " + mobOp.getName() + " su:");
		for (int i=0; i<postpaidTarife.size(); i++) {
			PostpaidTarifa tarifa = (PostpaidTarifa)postpaidTarife.get(i);
			if (tarifa.getMobilniOperater().getName().equals(mobOp.getName())) {
				System.out.println(tarifa.getTarifa());
			}
		}
		System.out.println();
	}
	
	public static PostpaidTarifa kreirajNovuPostpaidTarifu(MobilniOperater mobOp) {
		
		Scanner scannerString = new Scanner(System.in);
		Scanner scannerInt = new Scanner(System.in);
		
		System.out.println("unesi naziv nove postpaid tarife");
		String name = scannerString.nextLine();		
		System.out.println("da li je business? 1-yes / 2-no");
		int busines = scannerInt.nextInt();
		boolean business=true;
		if (busines==1) {
			business=true;
		} else if (busines==2) {
			business=false;
		}
		System.out.println("unesi broj minuta");
		int minuta = scannerInt.nextInt();
		System.out.println("unesi broj poruka");
		int poruka = scannerInt.nextInt();
		System.out.println("unesi broj mb");
		int mb = scannerInt.nextInt();
		
//		scannerString.close();
//		scannerInt.close();
		
		PostpaidTarifa postpaidTarifa = new PostpaidTarifa(name, mobOp, business, minuta, poruka, mb);
		postpaidTarife.add(postpaidTarifa);
		
		Main.postTar = postpaidTarifa;
		
		System.out.println(postpaidTarifa.toString());
		System.out.println(postpaidTarife.toString());
		System.out.println();
		
		return postpaidTarifa;
	}
	
	public static PostpaidTarifa ucitajPostpaidTarifu(MobilniOperater mobOp) {
		izlistajPostpaidTarife(mobOp);
		System.out.println("izaberi postpaid tarife");
		Scanner scannerString = new Scanner(System.in);
		String name = scannerString.nextLine();
		PostpaidTarifa postpaidTarifa = matchujPostpaidTarifu(name);
		Main.postTar = postpaidTarifa;
		System.out.println();
		return postpaidTarifa;
	}
	
	public int izracunajCenuTarife() {
		int ukupnaCenaMinuta = minuta * mobilniOperater.getMinPrice();
		int ukupnaCenaPoruka = poruka * mobilniOperater.getMessagePrice();
		int ukupnaCenaMb = mb * mobilniOperater.getMbPrice();
		int totalCenaTarife = ukupnaCenaMinuta + ukupnaCenaPoruka + ukupnaCenaMb;
		return totalCenaTarife;
	}

}
