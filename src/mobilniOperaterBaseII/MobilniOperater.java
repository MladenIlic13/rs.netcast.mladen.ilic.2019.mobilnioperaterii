package mobilniOperaterBaseII;

import java.util.ArrayList;
import java.util.Scanner;

public class MobilniOperater {
	
	public static ArrayList<MobilniOperater> mobilniOperateri = new ArrayList<>();
	
	public static ArrayList<String> log = new ArrayList<>();
	
	private ArrayList<TarifniDodatak> dodaci = new ArrayList<>();
	private String name;
	private int minPrice;
	private int messagePrice;
	private int mbPrice;
	
	public MobilniOperater() {
	}

	public MobilniOperater(ArrayList<TarifniDodatak> dodaci, String name, int minPrice, int messagePrice, int mbPrice) {
		this.dodaci = dodaci;
		this.name = name;
		this.minPrice = minPrice;
		this.messagePrice = messagePrice;
		this.mbPrice = mbPrice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}

	public int getMessagePrice() {
		return messagePrice;
	}

	public void setMessagePrice(int messagePrice) {
		this.messagePrice = messagePrice;
	}

	public int getMbPrice() {
		return mbPrice;
	}

	public void setMbPrice(int mbPrice) {
		this.mbPrice = mbPrice;
	}

	public ArrayList<TarifniDodatak> getDodaci() {
		return dodaci;
	}

	public void setDodaci(ArrayList<TarifniDodatak> dodaci) {
		this.dodaci = dodaci;
	}

	// dodaci u toString-u izazivaju stackOverflow (loop) error
	@Override
	public String toString() {
		return "MobilniOperater [name=" + name + ", minPrice=" + minPrice + ", messagePrice="
				+ messagePrice + ", mbPrice=" + mbPrice + "]";
	}
	
	// @@@@@@@@@@@@@@
	// logic
	
//	public static <E> String proveraDuplikata(ArrayList<E> arrayProvera) {
//		String proveraIme;
//		Scanner scannerString = new Scanner(System.in);
//		Loop:
//		while(true) {
//			System.out.println("unesi naziv za proveru");
//			proveraIme = scannerString.nextLine();
//			if (arrayProvera.size()==0) {
//				break Loop;
//			}
//			for (int i=0; i<arrayProvera.size(); i++) {
//				E arrayComponent = (E)arrayProvera.get(i);
//				if (arrayComponent.name.equals(proveraIme)) {
//					System.out.println("duplikat");
//				} else {
//					break Loop;
//				}
//			}
//		}
//	}
	
	public static void izlistajMobilneOperatere() {
		System.out.println("dostupni mobilni operateri su:");
		for (int i=0; i<mobilniOperateri.size(); i++) {
			MobilniOperater mobilniOperater = (MobilniOperater)mobilniOperateri.get(i);
			System.out.println(mobilniOperater.getName());
		}
		System.out.println();
	}
	
	// izbor mobilnog operatera
	public static MobilniOperater matchujMobilnogOperatera(String matchIme) {
		MobilniOperater mobilniOperater = null;
		for (int i=0; i<MobilniOperater.mobilniOperateri.size(); i++) {
			mobilniOperater = (MobilniOperater)MobilniOperater.mobilniOperateri.get(i);
			if (mobilniOperater.getName()==matchIme) {
				break;
			}
		}
		return mobilniOperater;
	}
	
	public static MobilniOperater dodajMobilnogOperatera() {
		
		Scanner scannerString = new Scanner(System.in);
		Scanner scannerInt = new Scanner(System.in);
		
		System.out.println("unesi naziv mobilnog operatora");
		String name = scannerString.nextLine();		
		System.out.println("unesi cenu minuta");
		int minut = scannerInt.nextInt();
		System.out.println("unesi cenu poruke");
		int poruka = scannerInt.nextInt();
		System.out.println("unesi cenu mb");
		int mb = scannerInt.nextInt();
		
//		scannerString.close();
//		scannerInt.close();
		
		ArrayList<TarifniDodatak> dodaci = new ArrayList<>();
		
		MobilniOperater mobilniOperater = new MobilniOperater(dodaci, name, minut, poruka, mb);
		mobilniOperateri.add(mobilniOperater);
		
		System.out.println(mobilniOperater.toString());
		System.out.println(mobilniOperateri.toString());
		System.out.println();
		
		return mobilniOperater;

	}
	
	public static MobilniOperater ucitajMobilnogOperatera() {
		MobilniOperater.izlistajMobilneOperatere();
		System.out.println("unesi naziv mobilnog operatora");
		Scanner scannerString = new Scanner(System.in);
		String name = scannerString.nextLine();
		MobilniOperater mobilniOperater = MobilniOperater.matchujMobilnogOperatera(name);
		Main.mobOp = mobilniOperater;
		System.out.println();
		return mobilniOperater;
	}
	
	public static void izlistajLog() {
		System.out.println("log:");
		for(String string : MobilniOperater.log) {
			System.out.println(string);
		}
		System.out.println();
	}
	
	public static void generisiPostpaidRacun(Korisnik korisnik) {
		int fiksnaCenaPaketa = korisnik.getPostpaidTarifa().izracunajCenuTarife();
		int ukupniDodaci = korisnik.getStanje();
		int total = fiksnaCenaPaketa + ukupniDodaci;
		System.out.println("postpaid racun svakog korisnika " + korisnik.getNaziv());
		System.out.println("stavke: fiksna cena tarife je " + fiksnaCenaPaketa 
				+ ", suma prekoracenja i dodataka je " + ukupniDodaci);
		System.out.println("ukupno: " + total);
		System.out.println();
	}
	
	public static void generisiPostpaidRacunPrivate(KorisnikPostpaidPrivate korisnik) {
		int fiksnaCenaPaketa = korisnik.getPostpaidTarifa().izracunajCenuTarife();
		int ukupniDodaci = korisnik.getStanje();
		int total = fiksnaCenaPaketa + ukupniDodaci;
		System.out.println("postpaid racun private korisnika " + korisnik.getNaziv());
		System.out.println("stavke: fiksna cena tarife je " + fiksnaCenaPaketa 
				+ ", suma prekoracenja i dodataka je " + ukupniDodaci);
		System.out.println("ukupno: " + total);
		System.out.println();
	}
	
	// ovaj vraca vrednost
	public static int generisiPostpaidRacunKratko(Korisnik korisnik) {
		int fiksnaCenaPaketa = korisnik.getPostpaidTarifa().izracunajCenuTarife();
		int ukupniDodaci = korisnik.getStanje();
		int total = fiksnaCenaPaketa + ukupniDodaci;
		return total;
	}
	
	// za operatera
	public static void zaposleniPoFirmama(MobilniOperater mobOp) {
		for (Firma firma : Firma.firme) {
			if (firma.getMobilniOperater().equals(mobOp)) {
				System.out.println("kompanija " + firma.getName());
				int broj = 0;
				for (KorisnikPostpaidBusiness korisnik : KorisnikPostpaidBusiness.postpaidKorisniciBusiness) {
					if (korisnik.getFirma().equals(firma)) {
						System.out.println("    korisnik " + korisnik.getNaziv());
						broj++;
					}
				}
				System.out.println("ukupno korisnika u firmi " + broj);
				System.out.println();
			}
		}
	}
	
	public static void racunZaFirmu(Firma firma) {
		System.out.println("racun za firmu " + firma.getName());
		int brojZaposlenih = 0;
		int ukupanRacun = 0;
		for (KorisnikPostpaidBusiness korisnik : KorisnikPostpaidBusiness.postpaidKorisniciBusiness) {
			if (korisnik.getFirma().equals(firma)) {
				int pojedinacniRacun = generisiPostpaidRacunKratko(korisnik);
				System.out.println("    korisnik " + korisnik.getNaziv() + ", iznos racuna " + pojedinacniRacun);
				brojZaposlenih++;
				ukupanRacun = ukupanRacun + pojedinacniRacun;
			}
		}
		System.out.println("totalno zaposlenih " + brojZaposlenih + ", vrednost ukupno " + ukupanRacun);
		System.out.println();
	}
	
	public static void sviRacuni(MobilniOperater mobOp) {
		System.out.println("<<svi racuni>>");
		System.out.println();
		for (KorisnikPostpaidPrivate korisnik : KorisnikPostpaidPrivate.postpaidKorisniciPrivate) {
			generisiPostpaidRacunPrivate(korisnik);
		} for (Firma firma : Firma.firme) {
			racunZaFirmu(firma);
		}
		System.out.println();
	}
}
