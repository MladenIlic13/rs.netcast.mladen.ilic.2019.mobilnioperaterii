package mobilniOperaterBaseII;

import java.util.ArrayList;
import java.util.Scanner;

public class KorisnikPostpaidPrivate extends Korisnik implements Tarifa {
	
	public static ArrayList<KorisnikPostpaidPrivate> postpaidKorisniciPrivate = new ArrayList<>();

	public KorisnikPostpaidPrivate() {
	}

	public KorisnikPostpaidPrivate(String naziv, int minuta, int poruka, int mbSurf, int stanje,
			PostpaidTarifa postpaidTarifa, ArrayList<TarifniDodatak> tarifniDodaci) {
		super(naziv, minuta, poruka, mbSurf, stanje, postpaidTarifa, tarifniDodaci);
	}

	@Override
	public String toString() {
		return "KorisnikPostpaidPrivate [tarifniDodaci=" + tarifniDodaci + "]";
	}

	// @@@@@@@@@@@@@@
	// logic
	
	public static KorisnikPostpaidPrivate dodajPostpaidKorisnikaPrivate(PostpaidTarifa tarifa) {
		
		if (Main.postTar.isBusiness()==true) {
			System.out.println("izaberite privatnu tarifu");
			System.out.println();
			return null;
		} else {
			Scanner scannerString = new Scanner(System.in);
			
			// naziv proveri duplikat
			String korisnikIme = null;
			Loop:
			while(true) {
				System.out.println("unesi naziv postpaid private korisnika");
				korisnikIme = scannerString.nextLine();
				if (postpaidKorisniciPrivate.size()==0) {
					break Loop;
				}
				int x = 0;
				for (int i=0; i<KorisnikPostpaidPrivate.postpaidKorisniciPrivate.size(); i++) {
					KorisnikPostpaidPrivate korisnik = (KorisnikPostpaidPrivate)KorisnikPostpaidPrivate.postpaidKorisniciPrivate.get(i);
					if (korisnik.getNaziv().equals(korisnikIme)) {
						x++;
					} 
				} for (int i=0; i<KorisnikPostpaidBusiness.postpaidKorisniciBusiness.size(); i++) {
					KorisnikPostpaidBusiness korisnik = (KorisnikPostpaidBusiness)KorisnikPostpaidBusiness.postpaidKorisniciBusiness.get(i);
					if (korisnik.getNaziv().equals(korisnikIme)) {
						x++;
					} 
				} for (int i=0; i<KorisnikPrepaid.prepaidKorisnici.size(); i++) {
					KorisnikPrepaid korisnik = (KorisnikPrepaid)KorisnikPrepaid.prepaidKorisnici.get(i);
					if (korisnik.getNaziv().equals(korisnikIme)) {
						x++;
					} 
				}
				if (x == 0) {
					break;
				} else {
					System.out.println("duplikat, unesite ponovo");
					continue;
				} 
			}
			
	//		scannerString.close();
			
			int minuta = tarifa.getMinuta();
			int poruka = tarifa.getPoruka();
			int mb = tarifa.getMb();
			int stanje = 0;
			ArrayList<TarifniDodatak> tarifniDodaci = new ArrayList<>();
	
			KorisnikPostpaidPrivate korisnikPostpaidPrivate = new KorisnikPostpaidPrivate(korisnikIme, minuta, poruka, mb, stanje, tarifa, tarifniDodaci);
			postpaidKorisniciPrivate.add(korisnikPostpaidPrivate);
			Main.korPostPri = korisnikPostpaidPrivate;
			
			System.out.println(korisnikPostpaidPrivate.toString());
			System.out.println(postpaidKorisniciPrivate.toString());
			System.out.println();
	
			return korisnikPostpaidPrivate;
		}
	}
	
	// listaj za konkretnu tarifu
	public static void izlistajPostpaidKorisnikePrivateTar(PostpaidTarifa tarifa) {
		System.out.println("korisnici sa tarifom " + tarifa.getTarifa() + " su:");
		for (int i=0; i<postpaidKorisniciPrivate.size(); i++) {
			KorisnikPostpaidPrivate korisnik = (KorisnikPostpaidPrivate)postpaidKorisniciPrivate.get(i);
			if (korisnik.getPostpaidTarifa().getTarifa().equals(tarifa.getTarifa())) {
				System.out.println(korisnik.getNaziv());
			}
		}
		System.out.println();
	}
	
	// izbor korisnika
	public static KorisnikPostpaidPrivate matchujPostpaidKorisnika(String matchIme) {
		KorisnikPostpaidPrivate korisnik = null;
		for (int i=0; i<postpaidKorisniciPrivate.size(); i++) {
			korisnik = (KorisnikPostpaidPrivate)postpaidKorisniciPrivate.get(i);
			if (korisnik.getNaziv().equals(matchIme)) {
				break;
			}
		}
		return korisnik;
	}
	
	public static KorisnikPostpaidPrivate ucitajPostpaidKorisnikaPrivate(PostpaidTarifa tarifa) {
		izlistajPostpaidKorisnikePrivateTar(tarifa);
		System.out.println("izaberi postpaid private korisnika");
		Scanner scannerString = new Scanner(System.in);
		String name = scannerString.nextLine();
		KorisnikPostpaidPrivate korisnik = matchujPostpaidKorisnika(name);
		Main.korPostPri = korisnik;
		System.out.println();
		return korisnik;
	}

	@Override
	public boolean razgovor() {
		System.out.println("koliko zelite da razgoravate minuta");
		Scanner sc = new Scanner(System.in);
		int minuta = sc.nextInt();
		if (this.getMinuta() < minuta) {
			System.out.println("prekoracili ste tarifu, dodaje vam se na stanje");
			int minutaZaStanje = minuta - this.getMinuta();
			int cenaZaStanje = this.getPostpaidTarifa().getMobilniOperater().getMinPrice() * minutaZaStanje;
			this.setMinuta(0);
			this.setStanje(this.getStanje() + cenaZaStanje);
		} else {
			System.out.println("u granicama ste tarife");
			this.setMinuta(this.getMinuta() - minuta);
		}
		String string = "postpaid private korisnik " + this.getNaziv() + ", tarifa "
				+ this.getPostpaidTarifa().getTarifa() + ", operater "
				+ this.getPostpaidTarifa().getMobilniOperater().getName()
				+ ", minuta razgovora " + minuta;
		MobilniOperater.log.add(string);
		System.out.println("uspesno obavljen razgovor");
		System.out.println();
		return true;
	}

	@Override
	public boolean slanjePoruka() {
		int cenaPoruke = this.getPostpaidTarifa().getMobilniOperater().getMessagePrice();
		int besplatnePoruke = 0;
		for (TarifniDodatak tarifniDodatak : this.getTarifniDodaci()) {
			if (tarifniDodatak.getTarifniDodatak().equals("sms")) {
				besplatnePoruke = 1;
				break;
			} else {
				besplatnePoruke = 2;
			}
		}
		if (besplatnePoruke == 1) {
			System.out.println("imate besplatne poruke");
		}
		else if (this.getPoruka() <= 0) {
			System.out.println("nemate dovoljno kredita da posaljete poruku, bice vam dotato na stanje");
			this.setStanje(this.getStanje() + cenaPoruke);
		} else {
			System.out.println("imate poruke, vasa poruka ce biti poslata");
			this.setPoruka(this.getPoruka() - 1);
		}
		System.out.println("unesite poruku");
		Scanner sc = new Scanner(System.in);
		String poruka = sc.nextLine();
		String string = "postpaid private korisnik " + this.getNaziv() + ", tarifa "
				+ this.getPostpaidTarifa().getTarifa() + ", operater "
				+ this.getPostpaidTarifa().getMobilniOperater().getName()
				+ ", poslata poruka <<" + poruka + ">>";
		MobilniOperater.log.add(string);
		System.out.println("uspesno poslata poruka");
		System.out.println();
		return true;
	}

	@Override
	public boolean surf() {
		System.out.println("koliko mb zelite da potrosite");
		Scanner sc = new Scanner(System.in);
		int brojMb = sc.nextInt();
		System.out.println("unesite zeljeni url");
		Scanner scs = new Scanner(System.in);
		String url = scs.nextLine();
		int besplatanSurf = 0;
		for (TarifniDodatak tarifniDodatak : this.getTarifniDodaci()) {
			if (tarifniDodatak.getTarifniDodatak().equals("net")) {
				besplatanSurf = 1;
				break;
			} else {
				besplatanSurf = 2;
			}
		}
		if (besplatanSurf == 1) {
			System.out.println("imate besplatan surf");
		}
		else if (this.getMbSurf() < brojMb) {
			System.out.println("nemate dovoljno mb, bice vam preneseno na stanje");
			int mbZaStanje = brojMb - this.getMbSurf();
			int cenaSurfa = this.getPostpaidTarifa().getMobilniOperater().getMbPrice() * brojMb;
			this.setMbSurf(0);
			this.setStanje(this.getStanje() + cenaSurfa);
		} else {
			System.out.println("imate dovoljno mb na racunu");
			this.setMbSurf(this.getMbSurf() - brojMb);	
		}
		String string = "postpaid private korisnik " + this.getNaziv() + ", tarifa "
				+ this.getPostpaidTarifa().getTarifa() + ", operater "
				+ this.getPostpaidTarifa().getMobilniOperater().getName()
				+ ", posecen url <<" + url + ">>" + ", potroseno mb " + brojMb;
		MobilniOperater.log.add(string);
		System.out.println("uspesno posecen sajt");
		System.out.println();
		return true;
	}
	
	public TarifniDodatak kupiDodatak() {
		MobilniOperater mobilniOperater = this.getPostpaidTarifa().getMobilniOperater();
		TarifniDodatak.izlistajTarifneDodatke(mobilniOperater);
		System.out.println("unesite/izaberite tarifni dodatak");
		Scanner sc = new Scanner(System.in);
		String dodatak = sc.nextLine();
		TarifniDodatak tarifniDodatak = TarifniDodatak.matchujTarifniDodatak(mobilniOperater, dodatak);
		this.setStanje(this.getStanje() + tarifniDodatak.getTarifniDodatakCena());
		this.getTarifniDodaci().add(tarifniDodatak);
		System.out.println();
		return tarifniDodatak;
	}
	
}
