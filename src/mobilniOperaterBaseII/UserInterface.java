package mobilniOperaterBaseII;

import java.util.ArrayList;

public class UserInterface {
	
	public static void hardcode() {
		
		MobilniOperater mobilniOperater = new MobilniOperater(new ArrayList<TarifniDodatak>(), "airtel", 3, 3, 3);
		MobilniOperater.mobilniOperateri.add(mobilniOperater);
		Main.mobOp = mobilniOperater;
		
		TarifniDodatak tarifniDodatak = new TarifniDodatak(Main.mobOp, "sms", 10);
		Main.mobOp.getDodaci().add(tarifniDodatak);
		
		Firma firma = new Firma("telekom", Main.mobOp);
		Firma.firme.add(firma);
		Main.fir = firma;
		
		PrepaidTarifa prepaidTarifa = new PrepaidTarifa("low", Main.mobOp, 30);
		PrepaidTarifa.prepaidTarife.add(prepaidTarifa);
		Main.preTar = prepaidTarifa;
		
		PostpaidTarifa postpaidTarifa = new PostpaidTarifa("high private", Main.mobOp, false, 30, 30, 30);
		PostpaidTarifa.postpaidTarife.add(postpaidTarifa);
		Main.postTar = postpaidTarifa;
		
		PostpaidTarifa postpaidTarifa2 = new PostpaidTarifa("high business", Main.mobOp, true, 30, 30, 30);
		PostpaidTarifa.postpaidTarife.add(postpaidTarifa2);
//		Main.postTar = postpaidTarifa2;
		
		KorisnikPrepaid korisnikPrepaid = new KorisnikPrepaid("tom", Main.preTar, Main.preTar.getPrepaidKredit());
		KorisnikPrepaid.prepaidKorisnici.add(korisnikPrepaid);
		Main.korPre = korisnikPrepaid;
		
		KorisnikPostpaidPrivate korisnikPostpaidPrivate = new KorisnikPostpaidPrivate("lela", Main.postTar.getMinuta(), Main.postTar.getPoruka(), Main.postTar.getMb(), 0, Main.postTar, new ArrayList<>());
		KorisnikPostpaidPrivate.postpaidKorisniciPrivate.add(korisnikPostpaidPrivate);
		Main.korPostPri = korisnikPostpaidPrivate;
		
		Main.postTar = postpaidTarifa2;
		KorisnikPostpaidBusiness korisnikPostpaidBusiness = new KorisnikPostpaidBusiness("li", Main.postTar.getMinuta(), Main.postTar.getPoruka(), Main.postTar.getMb(), 0, Main.postTar, new ArrayList<>(), Main.fir);
		KorisnikPostpaidBusiness.postpaidKorisniciBusiness.add(korisnikPostpaidBusiness);
		Main.korPostBus = korisnikPostpaidBusiness;
		
	}
	
	public static void status() {
		System.out.println("izabrane vrednosti: ");
		System.out.println();
		System.out.println("mobilni operater: " + Main.mobOp.getName());
		System.out.println("prepaid tarifa: " + Main.preTar.getTarifa());
		System.out.println("postpaid tarifa: " + Main.postTar.getTarifa());
		System.out.println("firma: " + Main.fir.getName());
		System.out.println("prepaid korisnik: " + Main.korPre.getNaziv());
		System.out.println("private postpaid korisnik: " + Main.korPostPri.getNaziv());
		System.out.println("business postpaid korisnik: " + Main.korPostBus.getNaziv());
//		System.out.println("genericki korisnik" + Main.korPost.getNaziv());
		System.out.println();
	}

}
