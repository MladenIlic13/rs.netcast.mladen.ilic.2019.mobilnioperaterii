package mobilniOperaterBaseII;

import java.util.Scanner;

public class Main {
	
	public static MobilniOperater mobOp;
	public static PrepaidTarifa preTar;
	public static PostpaidTarifa postTar;
	public static Firma fir;
	public static KorisnikPrepaid korPre;
	public static KorisnikPostpaidBusiness korPostBus;
	public static KorisnikPostpaidPrivate korPostPri;
	public static Korisnik korPost;
	
	public static void main(String[] args) {

		UserInterface.hardcode();
		
		while(true) {

			System.out.println("1 - dodaj mobilnog operatera");
			System.out.println("2 - izlistaj mobilnog operatera");
			System.out.println("3 - ucitaj mobilnog operatera");
			System.out.println();
			System.out.println("4 - dodaj tarifni dodatak");
			System.out.println("5 - izlistaj tarifne dodatke");
			System.out.println();
			System.out.println("6 - dodaj firmu");
			System.out.println("7 - izlistaj firme");
			System.out.println("8 - ucitaj firme");
			System.out.println();
			System.out.println("9 - kreiraj prepaid tarifu");
			System.out.println("10 - izlistaj prepaid tarife");
			System.out.println("11 - ucitaj prepaid tarifu");
			System.out.println();
			System.out.println("12 - kreiraj postpaid tarifu");
			System.out.println("13 - izlistaj postpaid tarife");
			System.out.println("14 - ucitaj postpaid tarifu");
			System.out.println();
			System.out.println("15 - dodaj prepaid korisnika");
			System.out.println("16 - izlistaj prepaid korisnika");
			System.out.println("17 - ucitaj prepaid korisnika");
			System.out.println();
			System.out.println("18 - dodaj postpaid private korisnika");
			System.out.println("19 - izlistaj postpaid private korisnika");
			System.out.println("20 - ucitaj postpaid private korisnika");
			System.out.println();
			System.out.println("21 - dodaj postpaid korisnika business");
			System.out.println("22 - izlistaj postpaid business korisnike po tarifi");
			System.out.println("23 - izlistaj postpaid business korisnike po firmi");
			System.out.println("24 - ucitaj postpaid korisnika business");
			System.out.println();
			System.out.println("25 - izlistaj generickog postpaid korisnika");
			System.out.println("26 - ucitaj generickog postpaid korisnika");
			System.out.println();
			System.out.println("27 - prepaid korisnik - razgovor");
			System.out.println("28 - prepaid korisnik - slanje poruke");
			System.out.println("29 - prepaid korisnik - surf");
			System.out.println();
			System.out.println("30 - postpaid private korisnik - razgovor");
			System.out.println("31 - postpaid private korisnik - slanje poruke");
			System.out.println("32 - postpaid private korisnik - surf");
			System.out.println("33 - postpaid private korisnik - kupi dodatak");
			System.out.println();
			System.out.println("34 - postpaid business korisnik - razgovor");
			System.out.println("35 - postpaid business korisnik - slanje poruke");
			System.out.println("36 - postpaid business korisnik - surf");
			System.out.println("37 - postpaid business korisnik - kupi dodatak");
			System.out.println();
			System.out.println("38 - izlistaj log");
			System.out.println("39 - generisi genericki postpaid racun ");
			System.out.println("40 - generisi postpaid private racun");
			System.out.println("41 - izlistaj zaposlene po firmama za izabranog operatera");
			System.out.println("42 - generisi racun za izabranu firmu");
			System.out.println("43 - generisi sve racune");
			System.out.println("44 - ugasi program");
			System.out.println();

			UserInterface.status();
			
			Scanner scannerInt = new Scanner(System.in);
			int c = scannerInt.nextInt();
	        switch (c) {
	            case (1): MobilniOperater.dodajMobilnogOperatera();break;
	            case (2): MobilniOperater.izlistajMobilneOperatere();break;
	            case (3): MobilniOperater.ucitajMobilnogOperatera();break;
	            
	            case (4): TarifniDodatak.dodajTarifniDodatak(Main.mobOp);break;
	            case (5): TarifniDodatak.izlistajTarifneDodatke(Main.mobOp);break;
	            
	            case (6): Firma.dodajFirmu(Main.mobOp);break;
	            case (7): Firma.izlistajFirme(Main.mobOp);break;
	            case (8): Firma.ucitajFirmu(Main.mobOp);break;
	            
	            case (9): PrepaidTarifa.kreirajNovuPrepaidTarifu(Main.mobOp);break;
	            case (10): PrepaidTarifa.izlistajPrepaidTarife(Main.mobOp);break;
	            case (11): PrepaidTarifa.ucitajPrepaidTarifu(Main.mobOp);break;
	            
	            case (12): PostpaidTarifa.kreirajNovuPostpaidTarifu(Main.mobOp);break;
	            case (13): PostpaidTarifa.izlistajPostpaidTarife(Main.mobOp);break;
	            case (14): PostpaidTarifa.ucitajPostpaidTarifu(Main.mobOp);break;
	            
	            case (15): KorisnikPrepaid.dodajPrepaidKorisnika(Main.preTar);break;
	            case (16): KorisnikPrepaid.izlistajPrepaidKorisnike(Main.preTar);break;
	            case (17): KorisnikPrepaid.ucitajPrepaidKorisnika(Main.preTar);break;
	            
	            case (18): KorisnikPostpaidPrivate.dodajPostpaidKorisnikaPrivate(Main.postTar);break;
	            case (19): KorisnikPostpaidPrivate.izlistajPostpaidKorisnikePrivateTar(Main.postTar);break;
	            case (20): KorisnikPostpaidPrivate.ucitajPostpaidKorisnikaPrivate(Main.postTar);break;
	            
	            case (21): KorisnikPostpaidBusiness.dodajPostpaidKorisnikaBusiness(Main.postTar, Main.fir);break;
	            case (22): KorisnikPostpaidBusiness.izlistajPostpaidKorisnikeBusinessTar(Main.postTar);break;
	            case (23): KorisnikPostpaidBusiness.izlistajPostpaidKorisnikeBusinessFir(Main.fir);break;
	            case (24): KorisnikPostpaidBusiness.ucitajPostpaidKorisnikaBusiness(Main.postTar, Main.fir);break;
	            
	            case (25): Korisnik.izlistajPostpaidKorisnike();break;
	            case (26): Korisnik.ucitajPostpaidKorisnika();break;
	            
	            case (27): Main.korPre.razgovor();break;
	            case (28): Main.korPre.slanjePoruka();break;
	            case (29): Main.korPre.surf();break;
	            
	            case (30): Main.korPostPri.razgovor();break;
	            case (31): Main.korPostPri.slanjePoruka();break;
	            case (32): Main.korPostPri.surf();break;
	            case (33): Main.korPostPri.kupiDodatak();break;
	            
	            case (34): Main.korPostBus.razgovor();break;
	            case (35): Main.korPostBus.slanjePoruka();break;
	            case (36): Main.korPostBus.surf();break;
	            case (37): Main.korPostBus.kupiDodatak();break;
	            
	            case (38): MobilniOperater.izlistajLog();break;
	            case (39): MobilniOperater.generisiPostpaidRacun(Main.korPost);break;
	            case (40): MobilniOperater.generisiPostpaidRacunPrivate(Main.korPostPri);break;
	            case (41): MobilniOperater.zaposleniPoFirmama(Main.mobOp);break;
	            case (42): MobilniOperater.racunZaFirmu(Main.fir);break;
	            case (43): MobilniOperater.sviRacuni(Main.mobOp);break;
	            
	            case (44): System.exit(0);
			
	            default:
	                System.out.println("greska");
	                break;
	        }
	
		}
		
	}
	
}
