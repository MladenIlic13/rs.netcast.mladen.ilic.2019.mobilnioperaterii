package mobilniOperaterBaseII;

import java.util.ArrayList;
import java.util.Scanner;

public class KorisnikPostpaidBusiness extends Korisnik implements Tarifa {
	
	public static ArrayList<KorisnikPostpaidBusiness> postpaidKorisniciBusiness = new ArrayList<>();

	private Firma firma;

	public KorisnikPostpaidBusiness() {
	}

	public KorisnikPostpaidBusiness(String naziv, int minuta, int poruka, int mbSurf, int stanje,
			PostpaidTarifa postpaidTarifa, ArrayList<TarifniDodatak> tarifniDodaci, Firma firma) {
		super(naziv, minuta, poruka, mbSurf, stanje, postpaidTarifa, tarifniDodaci);
		this.firma = firma;
	}

	public Firma getFirma() {
		return firma;
	}

	public void setFirma(Firma firma) {
		this.firma = firma;
	}

	@Override
	public String toString() {
		return "KorisnikPostpaidBusiness [firma=" + firma + ", tarifniDodaci=" + tarifniDodaci + "]";
	}

	// @@@@@@@@@@@@@@
	// logic
	
	public static KorisnikPostpaidBusiness dodajPostpaidKorisnikaBusiness(PostpaidTarifa tarifa, Firma firma) {
		
		if (Main.postTar.isBusiness()==false) {
			System.out.println("izaberite business tarifu");
			System.out.println();
			return null;
		} else {
			Scanner scannerString = new Scanner(System.in);
			
			// naziv proveri duplikat
			String korisnikIme = null;
			Loop:
			while(true) {
				System.out.println("unesi naziv postpaid business korisnika");
				korisnikIme = scannerString.nextLine();
				if (postpaidKorisniciBusiness.size()==0) {
					break Loop;
				}
				int x = 0;
				for (int i=0; i<KorisnikPostpaidPrivate.postpaidKorisniciPrivate.size(); i++) {
					KorisnikPostpaidPrivate korisnik = (KorisnikPostpaidPrivate)KorisnikPostpaidPrivate.postpaidKorisniciPrivate.get(i);
					if (korisnik.getNaziv().equals(korisnikIme)) {
						x++;
					} 
				} for (int i=0; i<KorisnikPostpaidBusiness.postpaidKorisniciBusiness.size(); i++) {
					KorisnikPostpaidBusiness korisnik = (KorisnikPostpaidBusiness)KorisnikPostpaidBusiness.postpaidKorisniciBusiness.get(i);
					if (korisnik.getNaziv().equals(korisnikIme)) {
						x++;
					} 
				} for (int i=0; i<KorisnikPrepaid.prepaidKorisnici.size(); i++) {
					KorisnikPrepaid korisnik = (KorisnikPrepaid)KorisnikPrepaid.prepaidKorisnici.get(i);
					if (korisnik.getNaziv().equals(korisnikIme)) {
						x++;
					} 
				}
				if (x == 0) {
					break;
				} else {
					System.out.println("duplikat, unesite ponovo");
					continue;
				}
			}
			
	//		scannerString.close();
			
			int minuta = tarifa.getMinuta();
			int poruka = tarifa.getPoruka();
			int mb = tarifa.getMb();
			int stanje = 0;
			ArrayList<TarifniDodatak> tarifniDodaci = new ArrayList<>();
	
			KorisnikPostpaidBusiness korisnikPostpaidBusiness = new KorisnikPostpaidBusiness(korisnikIme, minuta, poruka, mb, stanje, tarifa, tarifniDodaci, firma);
			postpaidKorisniciBusiness.add(korisnikPostpaidBusiness);
			Main.korPostBus = korisnikPostpaidBusiness;
			
			System.out.println(korisnikPostpaidBusiness.toString());
			System.out.println(postpaidKorisniciBusiness.toString());
			System.out.println();
	
			return korisnikPostpaidBusiness;
		}
	}
	
	// listaj za konkretnu tarifu
	public static void izlistajPostpaidKorisnikeBusinessTar(PostpaidTarifa tarifa) {
		System.out.println("korisnici sa tarifom " + tarifa.getTarifa() + " su:");
		for (int i=0; i<postpaidKorisniciBusiness.size(); i++) {
			KorisnikPostpaidBusiness korisnik = (KorisnikPostpaidBusiness)postpaidKorisniciBusiness.get(i);
			if (korisnik.getPostpaidTarifa().getTarifa().equals(tarifa.getTarifa())) {
				System.out.println(korisnik.getNaziv());
			}
		}
		System.out.println();
	}
	
	// listaj za konkretnu firmu
		public static void izlistajPostpaidKorisnikeBusinessFir(Firma firma) {
			System.out.println("korisnici zaposleni u firmi " + firma.getName() + " su:");
			for (int i=0; i<postpaidKorisniciBusiness.size(); i++) {
				KorisnikPostpaidBusiness korisnik = (KorisnikPostpaidBusiness)postpaidKorisniciBusiness.get(i);
				if (korisnik.getFirma().getName().equals(firma.getName())) {
					System.out.println(korisnik.getNaziv());
				}
			}
			System.out.println();
		}
	
	// izbor korisnika
	public static KorisnikPostpaidBusiness matchujPostpaidKorisnika(String matchIme) {
		KorisnikPostpaidBusiness korisnik = null;
		for (int i=0; i<postpaidKorisniciBusiness.size(); i++) {
			korisnik = (KorisnikPostpaidBusiness)postpaidKorisniciBusiness.get(i);
			if (korisnik.getNaziv().equals(matchIme)) {
				break;
			}
		}
		return korisnik;
	}
	
	public static KorisnikPostpaidBusiness ucitajPostpaidKorisnikaBusiness(PostpaidTarifa tarifa, Firma firma) {
		izlistajPostpaidKorisnikeBusinessTar(tarifa);
		izlistajPostpaidKorisnikeBusinessFir(firma);
		System.out.println("izaberi postpaid business korisnika");
		Scanner scannerString = new Scanner(System.in);
		String name = scannerString.nextLine();
		KorisnikPostpaidBusiness korisnik = matchujPostpaidKorisnika(name);
		Main.korPostBus = korisnik;
		System.out.println();
		return korisnik;
	}

	@Override
	public boolean razgovor() {
		System.out.println("koliko zelite da razgoravate minuta");
		Scanner sc = new Scanner(System.in);
		int minuta = sc.nextInt();
		if (this.getMinuta() < minuta) {
			System.out.println("prekoracili ste tarifu, dodaje vam se na stanje");
			int minutaZaStanje = minuta - this.getMinuta();
			int cenaZaStanje = this.getPostpaidTarifa().getMobilniOperater().getMinPrice() * minutaZaStanje;
			this.setMinuta(0);
			this.setStanje(this.getStanje() + cenaZaStanje);
		} else {
			System.out.println("u granicama ste tarife");
			this.setMinuta(this.getMinuta() - minuta);
		}
		String string = "postpaid business korisnik " + this.getNaziv() 
				+ ", zaposlen u firmi " + this.getFirma().getName() 
				+ ", tarifa " + this.getPostpaidTarifa().getTarifa() 
				+ ", operater " + this.getPostpaidTarifa().getMobilniOperater().getName()
				+ ", minuta razgovora " + minuta;
		MobilniOperater.log.add(string);
		System.out.println("uspesno obavljen razgovor");
		System.out.println();
		return true;
	}

	@Override
	public boolean slanjePoruka() {
		int cenaPoruke = this.getPostpaidTarifa().getMobilniOperater().getMessagePrice();
		int besplatnePoruke = 0;
		for (TarifniDodatak tarifniDodatak : this.getTarifniDodaci()) {
			if (tarifniDodatak.getTarifniDodatak().equals("sms")) {
				besplatnePoruke = 1;
				break;
			} else {
				besplatnePoruke = 2;
			}
		}
		if (besplatnePoruke == 1) {
			System.out.println("imate besplatne poruke");
		}
		else if (this.getPoruka() <= 0) {
			System.out.println("nemate dovoljno kredita da posaljete poruku, bice vam dotato na stanje");
			this.setStanje(this.getStanje() + cenaPoruke);
		} else {
			System.out.println("imate poruke, vasa poruka ce biti poslata");
			this.setPoruka(this.getPoruka() - 1);
		}
		System.out.println("unesite poruku");
		Scanner sc = new Scanner(System.in);
		String poruka = sc.nextLine();
		String string = "postpaid business korisnik " + this.getNaziv()
				+ ", zaposlen u firmi " + this.getFirma().getName() 
				+ ", tarifa " + this.getPostpaidTarifa().getTarifa() 
				+ ", operater " + this.getPostpaidTarifa().getMobilniOperater().getName()
				+ ", poslata poruka <<" + poruka + ">>";
		MobilniOperater.log.add(string);
		System.out.println("uspesno poslata poruka");
		System.out.println();
		return true;
	}

	@Override
	public boolean surf() {
		System.out.println("koliko mb zelite da potrosite");
		Scanner sc = new Scanner(System.in);
		int brojMb = sc.nextInt();
		System.out.println("unesite zeljeni url");
		Scanner scs = new Scanner(System.in);
		String url = scs.nextLine();
		int besplatanSurf = 0;
		for (TarifniDodatak tarifniDodatak : this.getTarifniDodaci()) {
			if (tarifniDodatak.getTarifniDodatak().equals("net")) {
				besplatanSurf = 1;
				break;
			} else {
				besplatanSurf = 2;
			}
		}
		if (besplatanSurf == 1) {
			System.out.println("imate besplatan surf");
		}
		else if (this.getMbSurf() < brojMb) {
			System.out.println("nemate dovoljno mb, bice vam preneseno na stanje");
			int mbZaStanje = brojMb - this.getMbSurf();
			int cenaSurfa = this.getPostpaidTarifa().getMobilniOperater().getMbPrice() * brojMb;
			this.setMbSurf(0);
			this.setStanje(this.getStanje() + cenaSurfa);
		} else {
			System.out.println("imate dovoljno mb na racunu");
			this.setMbSurf(this.getMbSurf() - brojMb);	
		}
		String string = "postpaid business korisnik " + this.getNaziv()
				+ ", zaposlen u firmi " + this.getFirma().getName() 
				+ ", tarifa " + this.getPostpaidTarifa().getTarifa() 
				+ ", operater " + this.getPostpaidTarifa().getMobilniOperater().getName()
				+ ", posecen url <<" + url + ">>" + ", potroseno mb " + brojMb;
		MobilniOperater.log.add(string);
		System.out.println("uspesno posecen sajt");
		System.out.println();
		return true;
	}
	
	public TarifniDodatak kupiDodatak() {
		MobilniOperater mobilniOperater = this.getPostpaidTarifa().getMobilniOperater();
		TarifniDodatak.izlistajTarifneDodatke(mobilniOperater);
		System.out.println("unesite/izaberite tarifni dodatak");
		Scanner sc = new Scanner(System.in);
		String dodatak = sc.nextLine();
		TarifniDodatak tarifniDodatak = TarifniDodatak.matchujTarifniDodatak(mobilniOperater, dodatak);
		this.setStanje(this.getStanje() + tarifniDodatak.getTarifniDodatakCena());
		this.getTarifniDodaci().add(tarifniDodatak);
		System.out.println();
		return tarifniDodatak;
	}
	
}
