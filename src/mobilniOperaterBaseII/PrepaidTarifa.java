package mobilniOperaterBaseII;

import java.util.ArrayList;
import java.util.Scanner;

public class PrepaidTarifa {

	public static ArrayList<PrepaidTarifa> prepaidTarife = new ArrayList<>();
	
	private String tarifa;
	
	private MobilniOperater mobilniOperater;
	
	private int prepaidKredit;

	public PrepaidTarifa() {
	}

	public PrepaidTarifa(String tarifa, MobilniOperater mobilniOperater, int prepaidKredit) {
		this.tarifa = tarifa;
		this.mobilniOperater = mobilniOperater;
		this.prepaidKredit = prepaidKredit;
	}

	public String getTarifa() {
		return tarifa;
	}

	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}

	public MobilniOperater getMobilniOperater() {
		return mobilniOperater;
	}

	public void setMobilniOperater(MobilniOperater mobilniOperater) {
		this.mobilniOperater = mobilniOperater;
	}

	public int getPrepaidKredit() {
		return prepaidKredit;
	}

	public void setPrepaidKredit(int prepaidKredit) {
		this.prepaidKredit = prepaidKredit;
	}

	@Override
	public String toString() {
		return "prepaidTarifa [tarifa=" + tarifa + ", mobilniOperater=" + mobilniOperater + ", prepaidKredit="
				+ prepaidKredit + "]";
	}
	
	// @@@@@@@@@@@@
	// logic
	
	// izbor tarife
	public static PrepaidTarifa matchujPrepaidTarifu(String matchIme) {
		PrepaidTarifa tarifa = null;
		for (int i=0; i<prepaidTarife.size(); i++) {
			tarifa = (PrepaidTarifa)prepaidTarife.get(i);
			if (tarifa.getTarifa().equals(matchIme)) {
				break;
			}
		}
		return tarifa;
	}
	
	// listaj za konkretnog operatera
	public static void izlistajPrepaidTarife(MobilniOperater mobOp) {
		System.out.println("dostupne prepaid tarife za " + mobOp.getName() + " su:");
		for (int i=0; i<prepaidTarife.size(); i++) {
			PrepaidTarifa tarifa = (PrepaidTarifa)prepaidTarife.get(i);
			if (tarifa.getMobilniOperater().getName().equals(mobOp.getName())) {
				System.out.println(tarifa.getTarifa());
			}
		}
		System.out.println();
	}
	
	public static PrepaidTarifa kreirajNovuPrepaidTarifu(MobilniOperater mobOp) {
		
		Scanner scannerString = new Scanner(System.in);
		Scanner scannerInt = new Scanner(System.in);
		
		System.out.println("unesi naziv nove prepaid tarife");
		String name = scannerString.nextLine();		
		System.out.println("unesi velicinu kredita din");
		int kredit = scannerInt.nextInt();
		
//		scannerString.close();
//		scannerInt.close();
		
		PrepaidTarifa prepaidTarifa = new PrepaidTarifa(name, mobOp, kredit);
		prepaidTarife.add(prepaidTarifa);
		
		Main.preTar = prepaidTarifa;
		
		System.out.println(prepaidTarifa.toString());
		System.out.println(prepaidTarife.toString());
		System.out.println();
		
		return prepaidTarifa;
	}
	
	public static PrepaidTarifa ucitajPrepaidTarifu(MobilniOperater mobOp) {
		izlistajPrepaidTarife(mobOp);
		System.out.println("izaberi prepaid tarife");
		Scanner scannerString = new Scanner(System.in);
		String name = scannerString.nextLine();
		PrepaidTarifa prepaidTarifa = matchujPrepaidTarifu(name);
		Main.preTar = prepaidTarifa;
		System.out.println();
		return prepaidTarifa;
	}
	
}
