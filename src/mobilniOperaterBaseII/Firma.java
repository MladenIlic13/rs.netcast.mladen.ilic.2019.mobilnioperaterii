package mobilniOperaterBaseII;

import java.util.ArrayList;
import java.util.Scanner;

public class Firma {

	public static ArrayList<Firma> firme = new ArrayList<>();
	
	private String name;
	private MobilniOperater mobilniOperater;

	public Firma() {
	}

	public Firma(String name) {
		this.name = name;
	}

	public Firma(String name, MobilniOperater mobilniOperater) {
		this.name = name;
		this.mobilniOperater = mobilniOperater;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MobilniOperater getMobilniOperater() {
		return mobilniOperater;
	}

	public void setMobilniOperater(MobilniOperater mobilniOperater) {
		this.mobilniOperater = mobilniOperater;
	}

	@Override
	public String toString() {
		return "Firma [name=" + name + ", mobilniOperater=" + mobilniOperater + "]";
	}
	
	// @@@@@@@@@@@@@@
	// logic
		
	public static Firma dodajFirmu(MobilniOperater mobOp) {
		
		Scanner scannerString = new Scanner(System.in);
		
		// proveri duplikat
		String firmaIme = null;
		Loop:
		while(true) {	
			System.out.println("unesi naziv kompanije");
			firmaIme = scannerString.nextLine();
			if (firme.size()==0) {
				break Loop;
			}
			int x = 0;
			for (int i=0; i<firme.size(); i++) {
				Firma firma = (Firma)firme.get(i);
				if (firma.getName().equals(firmaIme)) {
					x++;
				} 
			} 
			if (x == 0) {
				break;
			} else {
				System.out.println("duplikat, unesite ponovo");
				continue;
			}
		}
		
//		scannerString.close();

		Firma firma = new Firma(firmaIme, mobOp);
		firme.add(firma);
		Main.fir = firma;
		
		System.out.println(firma.toString());
		System.out.println(firme.toString());
		System.out.println();

		return firma;
		
	}
	
	// listaj za konkretnog operatera
	public static void izlistajFirme(MobilniOperater mobOp) {
		System.out.println("dostupne kompanije za " + mobOp.getName() + " su:");
		for (int i=0; i<firme.size(); i++) {
			Firma firma = (Firma)firme.get(i);
			if ((firma.getMobilniOperater().getName()).equals(mobOp.getName())) {
				System.out.println(firma.getName());
			}
		}
		System.out.println();
	}
	
	// izbor firme
	public static Firma matchujFirmu(String matchIme) {
		Firma firma = null;
		for (int i=0; i<firme.size(); i++) {
			firma = (Firma)firme.get(i);
			if (firma.getName().equals(matchIme)) {
				break;
			}
		}
		return firma;
	}
	
	public static Firma ucitajFirmu(MobilniOperater mobOp) {
		izlistajFirme(mobOp);
		System.out.println("unesi naziv kompanije");
		Scanner scannerString = new Scanner(System.in);
		String name = scannerString.nextLine();
		Firma firma = matchujFirmu(name);
		Main.fir = firma;
		System.out.println();
		return firma;
	}
	
}
